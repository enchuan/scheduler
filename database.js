var Sequelize = require('sequelize');
var config = require("./config");

console.log(config.mysql);
var database = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

var Performance = require("./performance.model.js")(database);


database
    .sync()
    .then(function () {
        console.log("Database in Sync Now");
    });

module.exports = {
    Performance: Performance
};

